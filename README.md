# SQL scripts for manuscript "Alternative methods for calculating optimal safety stock levels"

**REPOSITORY CITATION**

Mirzaee, Ashkan. Alternative methods for calculating optimal safety stock levels. Diss. University of Missouri-Columbia, 2017.

**RELATED PUBLICATION**

Mirzaee, Ashkan. Alternative methods for calculating optimal safety stock levels. Diss. University of Missouri-Columbia, 2017. URL https://mospace.umsystem.edu/xmlui/handle/10355/62067

## Access conditions
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.
Sourcecode is available under a [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Contents
SQL scripts to collect and trim input data from a large database and calculating optimal safety stock levels.

## Contact information
- Ashkan Mirzaee: amirzaee@mail.missouri.edu
