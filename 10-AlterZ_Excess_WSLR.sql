WITH input4 AS (
SELECT *
      ,[Total_Vol_WSLR] = Sum([Vol]) Over (Partition By [WSLR] Order by WSLR) 
      ,[RN] = Row_Number() Over (Partition by [WSLR], [Status] Order by WSLR)
      ,[#Records] = Count(*) Over (Partition by [WSLR] Order by WSLR)
      ,[#Shortfall/Excess_WSLR] = Count(*) Over (Partition by [WSLR], [Status] Order by WSLR)
	  ,[%Shortfall/Excess_WSLR] = (Count(*) Over (Partition by [WSLR], [Status] Order by WSLR) * 1.0) / (Count(*) Over (Partition by [WSLR] Order by WSLR) * 1.0)
  FROM [AB].[dbo].[SafetyStock]
 Where [Status] <> 'Unknown')

SELECT *    
      ,[Excess/Shortfall] = [Excess/Shortfall_Amount]
      ,[Excess/Shortfall_BBL] = [Excess/Shortfall_Vol]
      ,[#Excess] = [#Shortfall/Excess_WSLR]
      ,[Excess%] = [%Shortfall/Excess_WSLR] 
  INTO [AB].[dbo].[AlterZ_Excess_WSLR]
  FROM input4
 Where [%Shortfall/Excess_WSLR] > 0.9978 and 
       [Status] = 'Overstock'

GO
While (Select Max([Excess%]) 
       From [AB].[dbo].[AlterZ_Excess_WSLR]) > 0.9978
Begin
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Z] = CASE
                  When [Excess%] > 0.9978 Then Z - 0.5
		  Else Z End			
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [SS] = [Z] * SQRT(Mu_L * POWER(Sigma_D,2) + POWER(Mu_D,2) * POWER(Sigma_L,2))
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Target_Inventory] = SS + Expected_Demand 
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Status] = CASE  
                       WHEN CEILING(Target_Inventory) >= CEILING(Actual_Demand) Then 'OverStock'
                       ELSE 'Shortfall' END
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Excess/Shortfall] = ABS(CEILING(Target_Inventory) - CEILING(Actual_Demand))
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Excess/Shortfall_BBL] = [Excess/Shortfall]/CASES_BBL; 

	 With Excess_Update AS (
	 Select [WSLR] , Status,
	        Count(CASE When [Status] = 'OverStock' Then 1 Else 0 End) AS [Excess_num]
			From [AB].[dbo].[AlterZ_Excess_WSLR]
			Where Status= 'OverStock'
			Group by [WSLR], Status)		

	 Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [#Excess] = IIF([Excess_num] IS NULL, 0, [Excess_num])
            From [AB].[dbo].[AlterZ_Excess_WSLR]
  Left Join Excess_Update ON 
			Excess_Update.WSLR = [AB].[dbo].[AlterZ_Excess_WSLR].WSLR

     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Excess%] = [#Excess]*1.0/[#Records]*1.0

 IF (Select Max([Excess%]) 
       From [AB].[dbo].[AlterZ_Excess_WSLR]) <= 0.9978
      Break
    Else
  Continue 
End 

GO
While (Select Min([Excess%]) 
       From [AB].[dbo].[AlterZ_Excess_WSLR]) < 0.9978
Begin
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Z] = CASE
                  When [Excess%] < 0.9978 Then Z + 0.5
		  Else Z End			
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [SS] = [Z] * SQRT(Mu_L * POWER(Sigma_D,2) + POWER(Mu_D,2) * POWER(Sigma_L,2))
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Target_Inventory] = SS + Expected_Demand 
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Status] = CASE  
                       WHEN CEILING(Target_Inventory) >= CEILING(Actual_Demand) Then 'OverStock'
                       ELSE 'Shortfall' End
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Excess/Shortfall] = ABS(CEILING(Target_Inventory) - CEILING(Actual_Demand))
     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Excess/Shortfall_BBL] = [Excess/Shortfall]/CASES_BBL; 

	   With Excess_Update AS 
	(Select [WSLR] , Status,
	        Count(CASE When [Status] = 'OverStock' Then 1 Else 0 End) AS [Excess_num]
			From [AB].[dbo].[AlterZ_Excess_WSLR]
			Where Status= 'OverStock'
			Group by [WSLR], Status)		

	 Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [#Excess] = IIF([Excess_num] IS NULL, 0, [Excess_num])
            From [AB].[dbo].[AlterZ_Excess_WSLR]
			Left Join Excess_Update ON 
			Excess_Update.WSLR = [AlterZ_Excess_WSLR].WSLR

     Update [AB].[dbo].[AlterZ_Excess_WSLR] SET
            [Excess%] = [#Excess]*1.0/[#Records]*1.0

 IF (Select Min([Excess%]) 
       From [AB].[dbo].[AlterZ_Excess_WSLR]
	  Where [%Shortfall/Excess_WSLR] > 0.9978) >= 0.9978
      Break
    Else
  Continue 
End 

Go
SELECT [WSLR]
      ,CONVERT(float, CEILING ([Total_Vol_WSLR])) AS [Total_BBL_WSLR]
      ,[AvgExcess_ADJ] = CEILING (AVG([Excess/Shortfall_BBL]) Over (Partition By WSLR, Status Order By WSLR))
      ,[#Records]
      ,[#Shortfall/Excess_WSLR] AS [#Excess_Z=2.85]
      ,FORMAT([%Shortfall/Excess_WSLR], 'P') AS [%Excess_Z=2.85]
      ,CONVERT(DECIMAL(8,2),Z) AS [Z-value]
      ,[#Excess] AS [#Excess_Z-value]
      ,FORMAT([Excess%], 'P') AS [%Excess_Z-value]
  INTO [AB].[dbo].[AlterZ_Excess_WSLR_Results]
  FROM [AB].[dbo].[AlterZ_Excess_WSLR]
 Where [RN] = 1
 Order by [WSLR]