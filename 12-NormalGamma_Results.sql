WITH input6 AS (
SELECT [WSLR]
      ,[TotalSales_BBL] = Sum([SalesQty_BBL]) Over (Partition By [WSLR] Order by WSLR) 
      ,[Status_Norm]
      ,[AvgExcess_Norm] = Ceiling(Avg([Inv.N-AD]) Over (Partition By [WSLR], [Status_Norm] Order by WSLR))
      ,[RN2] = Row_Number() Over (Partition by [WSLR], [Status_Norm] Order by WSLR)
      ,[#Records] = Count(*) Over (Partition by [WSLR] Order by WSLR)
      ,[#Excess_Norm] = Count(*) Over (Partition by [WSLR], [Status_Norm] Order by WSLR)
      ,[ServiceLevel_Norm] = Format((Count(*) Over (Partition by [WSLR], [Status_Norm] Order by WSLR) * 1.0) / (Count(*) Over (Partition by [WSLR] Order by WSLR) * 1.0), 'P')
  FROM [AB].[dbo].[SafetyStockNormGama])

SELECT *
  INTO [AB].[dbo].[Normal_Over]
  FROM input6
 WHERE RN2 = 1 AND [Status_Norm] = 'Overstock'
;

WITH input7 AS (
SELECT [WSLR]
      ,[Status_Norm]
      ,[AvgShort_Norm] = Ceiling(Avg([Inv.N-AD]) Over (Partition By [WSLR], [Status_Norm] Order by WSLR))
      ,[RN2] = Row_Number() Over (Partition by [WSLR], [Status_Norm] Order by WSLR)
  FROM [AB].[dbo].[SafetyStockNormGama])

SELECT *
  INTO [AB].[dbo].[Normal_Short]
  FROM input7
 WHERE RN2 = 1 AND [Status_Norm] = 'Shortfall'

SELECT [Normal_Over].[WSLR]
      ,[TotalSales_BBL]
      ,[AvgExcess_Norm]
      ,[#Records]
      ,[#Excess_Norm]
      ,[ServiceLevel_Norm]
      ,[AvgShort_Norm]
  INTO [AB].[dbo].[Normal_Results]
  FROM [AB].[dbo].[Normal_Over]

LEFT JOIN 
       [AB].[dbo].[Normal_Short] ON
       [AB].[dbo].[Normal_Short].WSLR = [AB].[dbo].[Normal_Over].WSLR
;

WITH input8 AS (
SELECT [WSLR] 
      ,[Status_Gama]
      ,[AvgExcess_Gama] = Ceiling(Avg([Inv.G-AD]) Over (Partition By [WSLR], [Status_Gama] Order by WSLR))
      ,[RN2] = Row_Number() Over (Partition by [WSLR], [Status_Gama] Order by WSLR)
      ,[#Records] = Count(*) Over (Partition by [WSLR] Order by WSLR)
      ,[#Excess_Gama] = Count(*) Over (Partition by [WSLR], [Status_Gama] Order by WSLR)
      ,[ServiceLevel_Gama] = Format((Count(*) Over (Partition by [WSLR], [Status_Gama] Order by WSLR) * 1.0) / (Count(*) Over (Partition by [WSLR] Order by WSLR) * 1.0), 'P')
  FROM [AB].[dbo].[SafetyStockNormGama])

SELECT *
  INTO [AB].[dbo].[Gamma_Over]
  FROM input8
 WHERE RN2 = 1 AND [Status_Gama] = 'Overstock'
;

WITH input9 AS (
SELECT [WSLR]
      ,[Status_Gama]
      ,[AvgShort_Gama] = Ceiling(Avg([Inv.G-AD]) Over (Partition By [WSLR], [Status_Gama] Order by WSLR))
      ,[RN2] = Row_Number() Over (Partition by [WSLR], [Status_Gama] Order by WSLR)
  FROM [AB].[dbo].[SafetyStockNormGama])

SELECT *
  INTO [AB].[dbo].[Gamma_Short]
  FROM input9
 WHERE RN2 = 1 AND [Status_Gama] = 'Shortfall'

SELECT [Gamma_Over].[WSLR]
      ,[AvgExcess_Gama]
      ,[#Records]
      ,[#Excess_Gama]
      ,[ServiceLevel_Gama]
      ,[AvgShort_Gama]
  INTO [AB].[dbo].[Gamma_Results]
  FROM [AB].[dbo].[Gamma_Over]

LEFT JOIN 
       [AB].[dbo].[Gamma_Short] ON
       [AB].[dbo].[Gamma_Short].WSLR = [AB].[dbo].[Gamma_Over].WSLR
;

WITH input10 AS (
SELECT [WSLR]
      ,[Z-value_ADJ]
      ,[Status_ADJ]
      ,[AvgExcess_ADJ] = Ceiling(Avg([Inv.ADJ-AD]) Over (Partition By [WSLR], [Status_ADJ] Order by WSLR))
      ,[RN2] = Row_Number() Over (Partition by [WSLR], [Status_ADJ] Order by WSLR)
      ,[#Records] = Count(*) Over (Partition by [WSLR] Order by WSLR)
      ,[#Excess_ADJ] = Count(*) Over (Partition by [WSLR], [Status_ADJ] Order by WSLR)
      ,[ServiceLevel_ADJ] = Format((Count(*) Over (Partition by [WSLR], [Status_ADJ] Order by WSLR) * 1.0) / (Count(*) Over (Partition by [WSLR] Order by WSLR) * 1.0), 'P')
  FROM [AB].[dbo].[SafetyStockNormGama])

SELECT *
  INTO [AB].[dbo].[ADJ_Over]
  FROM input10
 WHERE RN2 = 1 AND [Status_ADJ] = 'Overstock'
;

WITH input11 AS (
SELECT [WSLR]
      ,[Status_ADJ]
      ,[AvgShort_ADJ] = Ceiling(Avg([Inv.ADJ-AD]) Over (Partition By [WSLR], [Status_ADJ] Order by WSLR))
      ,[RN2] = Row_Number() Over (Partition by [WSLR], [Status_ADJ] Order by WSLR)
  FROM [AB].[dbo].[SafetyStockNormGama])

SELECT *
  INTO [AB].[dbo].[ADJ_Short]
  FROM input11
 WHERE RN2 = 1 AND [Status_ADJ] = 'Shortfall'

SELECT [ADJ_Over].[WSLR]
      ,[Z-value_ADJ]
      ,[AvgExcess_ADJ]
      ,[#Records]
      ,[#Excess_ADJ]
      ,[ServiceLevel_ADJ]
      ,[AvgShort_ADJ]
  INTO [AB].[dbo].[ADJ_Results]
  FROM [AB].[dbo].[ADJ_Over]

LEFT JOIN 
       [AB].[dbo].[ADJ_Short] ON
       [AB].[dbo].[ADJ_Short].WSLR = [AB].[dbo].[ADJ_Over].WSLR
;

SELECT [Normal_Results].[WSLR]
      ,[Normal_Results].[TotalSales_BBL]
      ,[Normal_Results].[#Records]
      ,[AvgExcess_Norm]
      ,[AvgShort_Norm] = IIF([AvgShort_Norm] IS NULL, 0, [AvgShort_Norm])
      ,[#Excess_Norm]
      ,[ServiceLevel_Norm]
      ,[Z-value_ADJ]
      ,[AvgExcess_ADJ]
      ,[AvgShort_ADJ] = IIF([AvgShort_ADJ] IS NULL, 0, [AvgShort_ADJ])
      ,[#Excess_ADJ]
      ,[ServiceLevel_ADJ]
      ,[AvgExcess_Gama]
      ,[AvgShort_Gama] = IIF([AvgShort_Gama] IS NULL, 0, [AvgShort_Gama])
      ,[#Excess_Gama]
      ,[ServiceLevel_Gama]
  INTO [AB].[dbo].[NormalGamma_Results]
  FROM [AB].[dbo].[Normal_Results]

LEFT JOIN 
       [AB].[dbo].[Gamma_Results] ON
       [AB].[dbo].[Gamma_Results].WSLR = [Normal_Results].WSLR
LEFT JOIN 
       [AB].[dbo].[ADJ_Results] ON
       [AB].[dbo].[ADJ_Results].WSLR = [Normal_Results].WSLR
