WITH dint AS (
SELECT WSLR
      ,GPDCN
      ,Week_Number
      ,SalesQty
      ,[Integer]
  FROM [AB].[dbo].[SS_Input])

SELECT CW.WSLR
      ,CW.GPDCN
      ,CW.Week_Number
      ,CW.SalesQty
      ,CW.[Integer]
      ,Sum(BW.SalesQty) AS [ActualDemand_Int]
  INTO [AB].[dbo].[ActualDemand_Integer]
  FROM dint AS CW

INNER JOIN 
       dint AS BW ON
       BW.GPDCN = CW.GPDCN AND
       BW.WSLR = CW.WSLR AND
       CW.Week_Number - 2 >  BW.Week_Number  AND
       BW.Week_Number >= CW.Week_Number - CW.[Integer] - 2
 
 GROUP BY  
       CW.WSLR
      ,CW.GPDCN 
      ,CW.Week_Number
      ,CW.SalesQty
      ,CW.[Integer]
 
 ORDER BY 
       CW.WSLR
      ,CW.GPDCN
      ,CW.Week_Number
   ;

WITH input2 AS ( 
Select [Date]
      ,[SS_Input].[WSLR]
      ,[SS_Input].[GPDCN]
      ,[Brwy]
      ,[Wsc_Dc]
      ,[WSLR_NUM]
      ,[SS_Input].[SalesQty]
      ,[AvgSalesQty]
      ,[Fcst1]
      ,[Fcst2]
      ,[Fcst3]
      ,[Fcst4]
      ,[Fcst5]
      ,[Fcst6]
      ,[RMSE] = CONVERT(DECIMAL(10,3),[RMSE])
      ,[CASES_BBL] = CONVERT(DECIMAL(10,3),[CASES_BBL])
      ,[Vol] = CONVERT(DECIMAL(10,3),[VOL])
      ,[T1] = CONVERT(DECIMAL(10,3),[T1])
      ,[T1_sd] = CONVERT(DECIMAL(10,3),[T1_sd])
      ,[T2] = CONVERT(DECIMAL(10,3),[T2])
      ,[T2_sd] = CONVERT(DECIMAL(10,3),[T2_sd])
      ,[T3] = CONVERT(DECIMAL(10,3),[T3])
      ,[T3_sd] = CONVERT(DECIMAL(10,3),[T3_sd])
      ,[Mon_Split]
      ,[Tue_Split]
      ,[Wed_Split]
      ,[Thu_Split]
      ,[Fri_Split]
      ,[Cycle]
      ,[AC_Final]
      ,[SS_Input].[Week_Number]
      ,[Mu_L] = CONVERT(DECIMAL(10,3),[Mu_L])
      ,[Sigma_L] = CONVERT(DECIMAL(10,3),[Sigma_L])
      ,[Sigma_D] = CONVERT(DECIMAL(10,3),[Sigma_D])
      ,[SS_Input].[Integer]
      ,[Remainder] = CONVERT(DECIMAL(10,3),[Remainder])
      ,[ActualDemand_Int]
FROM [AB].[dbo].[SS_Input]

LEFT JOIN 
[AB].[dbo].[ActualDemand_Integer] ON
[AB].[dbo].[ActualDemand_Integer].WSLR = [AB].[dbo].[SS_Input].WSLR AND
[AB].[dbo].[ActualDemand_Integer].GPDCN = [AB].[dbo].[SS_Input].GPDCN AND
[AB].[dbo].[ActualDemand_Integer].Week_Number = [AB].[dbo].[SS_Input].Week_Number)

SELECT *, Z = CONVERT(float,2.85),
CASE
WHEN [Integer] >= 4 THEN 
     (LAG(Fcst3, 3, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst4, 4, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst5, 5, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst6, 6, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst6, 6, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) * (([Mu_L] - 28)/7))/[Mu_L]

WHEN [Integer] = 3 THEN  
     (LAG(Fcst3, 3, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst4, 4, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst5, 5, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst6, 6, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number)*
	  IIF( Remainder <= 1, Remainder * Mon_Split, 
	  IIF( Remainder <= 2, Mon_Split + (Remainder-1) * Tue_Split,
	  IIF( Remainder <= 3, Mon_Split + Tue_Split + (Remainder-2) * Wed_Split,
	  IIF( Remainder <= 4, Mon_Split + Tue_Split + Wed_Split + (Remainder-3) * Thu_Split,
	  IIF( Remainder <= 5, Mon_Split + Tue_Split + Wed_Split + Thu_Split + (Remainder-4) * Fri_Split, 1 ))))))/[Mu_L]  

WHEN [Integer] = 2 THEN  
     (LAG(Fcst3, 3, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst4, 4, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst5, 5, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) *
	  IIF( Remainder <= 1, Remainder * Mon_Split, 
	  IIF( Remainder <= 2, Mon_Split + (Remainder-1) * Tue_Split,
	  IIF( Remainder <= 3, Mon_Split + Tue_Split + (Remainder-2) * Wed_Split,
	  IIF( Remainder <= 4, Mon_Split + Tue_Split + Wed_Split + (Remainder-3) * Thu_Split,
	  IIF( Remainder <= 5, Mon_Split + Tue_Split + Wed_Split + Thu_Split + (Remainder-4) * Fri_Split, 1 ))))))/[Mu_L]	            

WHEN [Integer] = 1 THEN  
     (LAG(Fcst3, 3, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) +
      LAG(Fcst4, 4, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) *
	  IIF( Remainder <= 1, Remainder * Mon_Split, 
	  IIF( Remainder <= 2, Mon_Split + (Remainder-1) * Tue_Split,
	  IIF( Remainder <= 3, Mon_Split + Tue_Split + (Remainder-2) * Wed_Split,
	  IIF( Remainder <= 4, Mon_Split + Tue_Split + Wed_Split + (Remainder-3) * Thu_Split,
	  IIF( Remainder <= 5, Mon_Split + Tue_Split + Wed_Split + Thu_Split + (Remainder-4) * Fri_Split, 1 ))))))/[Mu_L]	

ELSE (LAG(Fcst3, 3, 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number) * 
	  IIF( Remainder <= 1, Remainder * Mon_Split, 
	  IIF( Remainder <= 2, Mon_Split + (Remainder-1) * Tue_Split,
	  IIF( Remainder <= 3, Mon_Split + Tue_Split + (Remainder-2) * Wed_Split,
	  IIF( Remainder <= 4, Mon_Split + Tue_Split + Wed_Split + (Remainder-3) * Thu_Split,
	  IIF( Remainder <= 5, Mon_Split + Tue_Split + Wed_Split + Thu_Split + (Remainder-4) * Fri_Split, 1 ))))))/[Mu_L] 
END AS Mu_D,

CASE 
WHEN Remainder <= 1 THEN ActualDemand_Int + (LAG(SalesQty,[Integer]+3 , 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number))* Remainder * Mon_Split
WHEN Remainder <= 2 THEN ActualDemand_Int + (LAG(SalesQty,[Integer]+3 , 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number))* (Mon_Split + (Remainder-1)*Tue_Split)
WHEN Remainder <= 3 THEN ActualDemand_Int + (LAG(SalesQty,[Integer]+3 , 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number))* (Mon_Split + Tue_Split + (Remainder-2) * Wed_Split)
WHEN Remainder <= 4 THEN ActualDemand_Int + (LAG(SalesQty,[Integer]+3 , 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number))* (Mon_Split + Tue_Split + Wed_Split + (Remainder-3) * Thu_Split)
WHEN Remainder <= 5 THEN ActualDemand_Int + (LAG(SalesQty,[Integer]+3 , 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number))* (Mon_Split + Tue_Split + Wed_Split + Thu_Split + (Remainder-4) * Fri_Split)
ELSE ActualDemand_Int + (LAG(SalesQty,[Integer]+3 , 0) OVER (PARTITION BY Wslr, Gpdcn ORDER BY Week_Number))
END AS Actual_Demand

INTO [AB].[dbo].[SafetyStock]
FROM input2

Update [AB].[dbo].[SafetyStock] SET 
[Mu_D] = CONVERT(DECIMAL(10,3), Mu_D)

Update [AB].[dbo].[SafetyStock] SET 
[Actual_Demand] = CONVERT(DECIMAL(10,3), Actual_Demand)

ALTER TABLE [AB].[dbo].[SafetyStock] ADD [SS] float NULL 
GO 
Update [AB].[dbo].[SafetyStock] SET 
[SS] = CONVERT(DECIMAL(10,3), 2.85 * SQRT(Mu_L * POWER(Sigma_D,2) + POWER(Mu_D,2) * POWER(Sigma_L,2)))

ALTER TABLE [AB].[dbo].[SafetyStock] ADD [Expected_Demand] float NULL 
GO 
Update [AB].[dbo].[SafetyStock] SET 
[Expected_Demand] = CONVERT(DECIMAL(10,3), Mu_L * Mu_D)

ALTER TABLE [AB].[dbo].[SafetyStock] ADD [Target_Inventory] float NULL 
GO 
Update [AB].[dbo].[SafetyStock] SET 
[Target_Inventory] = SS + Expected_Demand

ALTER TABLE [AB].[dbo].[SafetyStock] ADD [Status] Varchar(50) NULL 
GO 
Update [AB].[dbo].[SafetyStock] SET [Status] = 
CASE
WHEN Week_Number > [Integer] + 3 THEN
     IIF (Actual_Demand IS NULL, 'Unknown', 
     IIF (RMSE IS NULL, 'Unknown',
     IIF (CEILING(Target_Inventory) >= CEILING(Actual_Demand), 'OverStock', 'Shortfall')))
ELSE 'Unknown'
END

ALTER TABLE [AB].[dbo].[SafetyStock] ADD [Excess/Shortfall_Amount] float NULL 
GO 
Update [AB].[dbo].[SafetyStock] SET 
[Excess/Shortfall_Amount] = ABS(CEILING(Target_Inventory) - CEILING(Actual_Demand))

ALTER TABLE [AB].[dbo].[SafetyStock] ADD [Excess/Shortfall_Vol] float NULL 
GO 
Update [AB].[dbo].[SafetyStock] SET 
[Excess/Shortfall_Vol] = CONVERT(DECIMAL(10,3), [Excess/Shortfall_Amount]/CASES_BBL)
